from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from .forms import AutorForm
from .models import Autor


# Create your views here.
def home(request):
    return render(request, 'libro/index.html')


@require_http_methods(["POST", "GET"])
def crear_autor(request):
    if request.method == 'POST':
        autor_form = AutorForm(request.POST)
        if autor_form.is_valid():
            autor_form.save()
            return redirect('index')
    else:
        autor_form = AutorForm()

    return render(request, 'libro/crear_autor.html', {
        'autor_form': autor_form
    })


@require_http_methods(['GET'])
def listar_autores(request):
    autores = Autor.objects.all()
    return render(request, 'libro/listar_autor.html', {
        'autores': autores
    })
